# Kali NetHunter Utility Package

[This repository](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-utils) includes various Kali NetHunter utility scripts for the [Kali NetHunter filesystem/chroot](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-project/-/tree/master/nethunter-fs).
Its package name is [nethunter-utils](https://pkg.kali.org/pkg/nethunter-utils).



Sat  1 Jun 2024 03:44:53 UTC
